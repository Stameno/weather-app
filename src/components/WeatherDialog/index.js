import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import List from '@material-ui/core/List';
import {Portal} from 'react-portal';

export default function WeatherDialog({title, items, onClose, open, selectedItem, children}) {

    const handleClose = () => {
        onClose(selectedItem);
    };

    const handleListItemClick = (value) => {
        onClose(value);
    };

    return (
        <Portal>
            <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
                <DialogTitle id="simple-dialog-title">{title}</DialogTitle>
                <List>
                    {items.map((item) => React.cloneElement(children, { item, handleListItemClick }))}
                </List>
            </Dialog>
        </Portal>
    );
}
