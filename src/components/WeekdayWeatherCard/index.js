import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import WeatherIcon from 'components/WeatherIcon';
import dayjs from 'dayjs';
import styles from 'components/WeekdayWeatherCard/style.module.css';

export default function WeekdayWeatherCard({forecast, onButtonClick}) {


    const {main, weather, dt} = forecast[0];

    return (
        <Card>
            {weather ? <>
            <CardContent>
                <WeatherIcon weather={weather[0].main}/>
                <div>{dayjs.unix(dt).subtract(3, 'hour').format('dddd')}</div>
                <div>{dayjs.unix(dt).subtract(3, 'hour').format('DD/MM/YY')}</div>
                <div>{main.temp_min.toFixed(0)} | {main.temp_max.toFixed(0)}</div>
            </CardContent>
            <CardActions className={styles.hourlyButtonContainer}>
                <Button
                    onClick={() => onButtonClick(forecast)}
                    variant="contained"
                    color="primary">
                    Hourly
                </Button>
            </CardActions>
            </> : null }
        </Card>
    );
}
