import ListItemText from '@material-ui/core/ListItemText/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import React from 'react';

export default function CityDialogItem({item, handleListItemClick}) {

    return(
        <ListItem button onClick={() => handleListItemClick(item)} key={item.id}>
            <ListItemText primary={item.name} secondary={item.country}/>
        </ListItem>
    )
}
