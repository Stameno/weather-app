import React from 'react';
import Clouds from 'assets/icons/cloudy.svg';
import Rain from 'assets/icons/rainy.svg';
import Sun from 'assets/icons/sunny.svg';
import Snow from 'assets/icons/snowy.svg';
import Wind from 'assets/icons/wind.svg';

export default function WeatherIcon({weather}) {

    function switchWeatherType(weatherType) {
        switch (weatherType) {
            case 'Rain':
                return Rain;
            case 'Clouds':
                return Clouds;
            case 'Sun':
                return Sun;
            case 'Snow':
                return Snow;
            case 'Wind':
                return Wind;
            default:
                return Sun
        }
    }

    return(
        <img src={switchWeatherType(weather)} alt="Logo" />
    )
}
