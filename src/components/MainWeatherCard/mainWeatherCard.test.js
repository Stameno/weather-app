import React from 'react';
import { render } from '@testing-library/react'

import MainWeatherCard from 'components/MainWeatherCard';

const setup = () => {
    const utils = render(<MainWeatherCard
        weatherData={{
            "dt": 1623013200,
            "main": {
                "temp": 17.68,
                "feels_like": 17.46,
                "temp_min": 14.61,
                "temp_max": 17.68,
                "pressure": 1017,
                "sea_level": 1017,
                "grnd_level": 949,
                "humidity": 75,
                "temp_kf": 3.07
            },
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
        }}
        city={{
            country: "BG",
            name: "Sofia-Grad",
        }}
    />);
    const cityName = utils.getByTestId('city-name');
    return {
        cityName,
        ...utils,
    }
};

test('It should allow the text to be added and deleted', () => {
    const { cityName } = setup();
    expect((cityName).textContent).toEqual('Sofia-Grad, BG')
})
