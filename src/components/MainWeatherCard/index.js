import React from 'react';
import styles from 'components/MainWeatherCard/style.module.css';
import WeatherIcon from 'components/WeatherIcon';
import dayjs from 'dayjs';

export default function MainWeatherCard({city, weatherData}) {

    const {name, country} = city;
    const {main, wind, dt, weather} = weatherData || {};

    return (
        <div className={styles.container}>
            <WeatherIcon weather={weather ? weather[0].main : ''}/>
            {name ? <p data-testid="city-name" className={styles.cityName}>{name}, {country}</p> :
                <p className={styles.cityName}>Please select a city</p>}
            {name ?
                <span>
                    <p>{dayjs.unix(dt).subtract(3, 'hour').format('dddd')}</p>
                    <p>{dayjs.unix(dt).subtract(3, 'hour').format('DD/MM/YY')}</p>
                    <p className={styles.temperature}>{main?.temp.toFixed(0)}℃</p>
                    <div className={styles.windContainer}>
                        <div className={styles.windIcon}>
                            <WeatherIcon weather={'Wind'}/>
                        </div>
                        {wind?.speed} km/h
                    </div>
                </span> : null}
        </div>
    )
}
