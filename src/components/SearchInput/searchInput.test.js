import React from 'react';
import { render, fireEvent } from '@testing-library/react'

import SearchInput from 'components/SearchInput';

const setup = () => {
    const utils = render(<SearchInput/>)
    const input = utils.getByLabelText('Find city')
    return {
        input,
        ...utils,
    }
};

test('It should allow the text to be added and deleted', () => {
    const { input } = setup();
    fireEvent.change(input, { target: { value: 'Sofia' } });
    expect(input.value).toBe('Sofia'); // need to make a change so React registers "" as a change
    fireEvent.change(input, { target: { value: '' } });
    expect(input.value).toBe('')
})
