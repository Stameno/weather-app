import React, {useState} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import MyLocationIcon from '@material-ui/icons/MyLocation';
import Grid from '@material-ui/core/Grid';
import styles from 'components/SearchInput/style.module.css';

export default function SearchInput({loading, onSearchClick, onGeolocationClick}) {

    const [inputValue, setInputValue] = useState('')

    return (
        <Grid className={styles.container} container justify="center" spacing={1}>
            <Grid item xs={7}>
                <TextField onChange={(event) => setInputValue(event.target.value)} id="standard-basic"
                           label="Find city"/>
            </Grid>
            <Grid item xs={3}>
                <Button onClick={() => onSearchClick(inputValue)} variant="contained" color="primary">
                    Find
                </Button>
            </Grid>
            <Grid item xs={2}>
                <Button onClick={onGeolocationClick} variant="contained" color="primary">
                    <MyLocationIcon/>
                </Button>
            </Grid>
        </Grid>
    )
}
