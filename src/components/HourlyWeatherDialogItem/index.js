import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import WeatherIcon from 'components/WeatherIcon';
import dayjs from 'dayjs';

export default function HourlyWeatherDialogitem({item}) {

    return(
        <ListItem key={item.dt}>
            <p>{dayjs.unix(item.dt).subtract(3, 'hour').format('HH:mm')}</p>
            <WeatherIcon weather={item.weather[0]?.main}/>
            <div>Max: {item.main?.temp_max.toFixed(0)}℃ | Feels like: {item.main?.feels_like.toFixed(0)}℃</div>
        </ListItem>
    )
}
