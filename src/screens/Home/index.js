import React, {useEffect, useState} from 'react'
import MainWeatherCard from 'components/MainWeatherCard';
import WeekdayWeatherCard from 'components/WeekdayWeatherCard';
import SearchInput from 'components/SearchInput';
import CityList from 'assets/data/city.list';
import WeatherDialog from 'components/WeatherDialog';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import styles from 'screens/Home/style.module.css';
import dayjs from 'dayjs';
import _ from 'lodash';
import CityDialogItem from 'components/CityDialogItem';
import HourlyWeatherDialogitem from 'components/HourlyWeatherDialogItem';

export default function Home() {

    const [lat, setLat] = useState('');
    const [lng, setLng] = useState('');
    const [filteredCities, setFilteredCities] = useState([]);
    const [cityDialogVisible, setCityDialogVisible] = useState(false);
    const [city, setCity] = useState({});
    const [weather, setWeather] = useState({});
    const [forecastByDay, setForecastByDay] = useState([]);
    const [forecastDialogVisible, setForecastDialogVisible] = useState(false);
    const [hourlyForecast, setHourlyForecast] = useState([]);

    function fetchLocation() {
        return navigator.geolocation.getCurrentPosition(function (position) {
            setLat(position.coords.latitude);
            setLng(position.coords.longitude);
        });
    }

    function fetchWeatherData(lat, lng) {
        return fetch(`${process.env.REACT_APP_API_URL}/forecast?lat=${lat}&lon=${lng}&cnt=200&units=metric&appid=${process.env.REACT_APP_API_KEY}`)
            .then(res => res.json())
    }

    /*
    * Makes a request to the openWeatherMap API every time the lat and lng are changed
    * On successful response sets the current forecast and the 5 day one
     */
    useEffect(() => {
        fetchWeatherData(lat, lng).then(res => {
            if(res.cod === "200") {
                setCity(res.city);
                setWeather(getWeather(res.list));
                setForecastByDay(groupByDay(res.list))
            }
        }).catch(err => console.log(err))
    }, [lat, lng]);

    /*
    * Receives the full forecast and if not undefined
    * Returns the current weather
     */
    function getWeather(weather) {
        if (weather) {
            return weather[0]
        }
    }

    /*
    * Receives a keyword from the text input component
    * Filters the array of cities provided by openWeatherMap
    * Sets the data for the city dialog and sets it to visible
     */
    function searchCities(keyword) {
        setFilteredCities(CityList.filter(city => city.name.toLowerCase().includes(keyword.toLowerCase())));
        setCityDialogVisible(true)
    }

    /*
    * Receives the selected city from the dialog
    * If the selected city has coordinates, sets new latitude and longitude
    * Finally it hides the city dialog
     */
    function onCitySelect(selectedCity) {
        setCity(selectedCity);
        if (selectedCity.coord) {
            setLat(selectedCity.coord?.lat);
            setLng(selectedCity.coord?.lon)
        }
        setCityDialogVisible(false)
    }

    function onHourlyDialogClose() {
        setForecastDialogVisible(false)
    }

    /*
    * Receives the full 5 day forecast and groups the hours by day based on an unix stamp
    * Returns an object containing the grouped forecast
     */
    function groupByDay(forecast) {
        return _.groupBy(forecast, (result) => dayjs.unix(result.dt).subtract(3, 'hour').format('DD'));
    }

    /*
    * Sets the data and makes the dialog for hourly forecast visible
     */
    function previewHourlyForecast(forecast) {
        setHourlyForecast(forecast);
        setForecastDialogVisible(true)
    }

    /*
    * Consumes the grouped forecast by day
    * Transforms the object to array
    * Slices the data for today
    * Returns the forecast cards for the week
     */
    function renderWeeklyForecast(forecastByDay) {
        let arr = [];
        for (let forecast in forecastByDay) {
            arr.push([forecast, forecastByDay[forecast]])
        }

        return arr.sort().slice(1).map(function(key) {
            return <Grid item><WeekdayWeatherCard
                forecast={key[1]}
                onButtonClick={previewHourlyForecast}
            /></Grid>
        });
    }

    return (
        <Container className={styles.mainContainer} maxWidth="md">
            <Grid container>
                <Grid className={styles.leftContainer} container item xs={9}>
                    <Grid item container xs={12}>
                        <Grid item xs={5}>
                            <h2>Weather Forecast</h2>
                        </Grid>
                        <Grid item xs={6}>
                            <SearchInput
                                onSearchClick={searchCities}
                                onGeolocationClick={fetchLocation}
                            />
                        </Grid>
                    </Grid>
                    <Grid container item xs={12} spacing={1}>
                        {renderWeeklyForecast(forecastByDay)}
                    </Grid>
                </Grid>
                <Grid item xs={3}>
                    <MainWeatherCard
                        city={city}
                        weatherData={weather}
                        onButtonClick={previewHourlyForecast}
                    />
                </Grid>
            </Grid>
            <WeatherDialog
                title={'Select a city'}
                items={filteredCities}
                onClose={onCitySelect}
                open={cityDialogVisible}
                selectedItem={city}
            >
                <CityDialogItem />
            </WeatherDialog>
            <WeatherDialog
                title={'Weather forecast hourly'}
                items={hourlyForecast}
                onClose={onHourlyDialogClose}
                open={forecastDialogVisible}
                selectedItem={''}
            >
                <HourlyWeatherDialogitem/>
            </WeatherDialog>
        </Container>
    )
}
